import { Component, OnInit } from '@angular/core';
import { TesteService } from '../teste.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements OnInit {
  
    active: boolean;
    avatarUrl: "https://picsum.photos/seed/picsum/200/300";
    nomeFantasia: string;
    qtdeFuncionarios: number;
    razaoSocial: string;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  terceiroFormGroup: FormGroup;

  constructor(private post: TesteService, private _formBuilder: FormBuilder, private rout: Router) { }

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required],
    });
    this.terceiroFormGroup = this._formBuilder.group({
      terceiroCtrl: ['', Validators.required],
    });
  }

  postTeste(nome:string, razaoSocial, qtdeFuncionarios){
    this.nomeFantasia = nome;
    this.razaoSocial = razaoSocial;
    this.qtdeFuncionarios = qtdeFuncionarios;

    this.post.postTeste({
      active: true,
      nomeFantasia: this.nomeFantasia,
      razaoSocial: this.razaoSocial,
      qtdeFuncionarios: this.qtdeFuncionarios,
      avatarUrl: this.avatarUrl
    }).subscribe(
      () => this.rout.navigate(['page1'])
    );

    console.log(nome, razaoSocial, qtdeFuncionarios)
  }

}
