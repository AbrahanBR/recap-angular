import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'app';
  navLinks: any[];
  activeLinkIndex = -1;

  constructor(private router: Router) {
    this.navLinks = [
        {
            label: 'TabTest1',
            link: 'page1',
            index: 0
        }, {
            label: 'Tab Test2',
            link: 'page2',
            index: 1
        }
    ];
}
ngOnInit(): void {
}

teste(link){
  console.log(link)
  this.router.navigate([link])
}

tabClick(tab) {
  let link: string;
  if(tab.index === 0){
    link = "page1"
  }else {
    link = "page2"
  }
  this.router.navigate([link])
  console.log(tab.index, link);
}

}
