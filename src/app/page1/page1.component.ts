import { Component, Inject, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TesteService } from '../teste.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss']
})
export class Page1Component implements OnInit {

  public title: any;
  public isLoading = true;
  public erro = false;

  constructor(private teste: TesteService, public dialog: MatDialog) {

  }

  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  ngOnInit(): void {
    this.teste1();
  }

  openDialog(id, name, qtdeFuncionarios, razaoSocial): void {
    const dialogRef = this.dialog.open(UpdateDialog, {
      width: '250px',
      data: {
        nomeFantasia: name,
        qtdeFuncionarios: qtdeFuncionarios,
        razaoSocial: razaoSocial
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.isLoading = true
        this.update(id, name, qtdeFuncionarios, razaoSocial)
      }
    });
  }

  teste1() {
    this.teste.getTeste().subscribe(
      (data: any) => {
        this.title = data
        this.isLoading = false
        console.log(this.title)
      },
      (err) => {
        this.isLoading = false
        this.erro = true
        console.log(err)

      })
  }

  update(id, name, qtdeFuncionarios, razaoSocial) {
    const data = {
      active: true,
      avatarUrl: "https://angular.io/assets/images/logos/angular/shield-large.svg",
      id: 5,
      nomeFantasia: name,
      qtdeFuncionarios: qtdeFuncionarios,
      razaoSocial: razaoSocial
    }
    this.teste.putTeste(id, data).subscribe(() => this.teste1())
  }

  deleteDialog(id, company) {
    const dialogRef = this.dialog.open(DeleteDialog, {
      width: '250px',
      data: {
        id: id,
        company: company
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.isLoading = true
        this.teste.deleteTeste(id).subscribe(() => this.teste1());
      }
    });
  }
}



@Component({
  selector: 'delete-dialog',
  templateUrl: 'delete-dialog.html',
})
export class DeleteDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}



@Component({
  selector: 'update-dialog',
  templateUrl: 'update-dialog.html',
})
export class UpdateDialog {
  constructor(
    public dialogRef: MatDialogRef<UpdateDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
