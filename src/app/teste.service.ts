import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TesteService {
  private URL = 'https://api.helena.app/test/api/Company';

  constructor(private http: HttpClient) { }


  getTeste(){
    return this.http.get<any>(this.URL);
  }

  postTeste(data:any){
    return this.http.post<any>(this.URL, data)
  }

  deleteTeste(id:any):Observable<any> {
    const URLdelete = `${this.URL}/${id}`;
    return this.http.delete(URLdelete)
  }

  putTeste(id:any,company:any){
    const URLupdate = `${this.URL}/${id}`;
    return this.http.put<any>(URLupdate, company)

  }
}
