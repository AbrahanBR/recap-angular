import { Component, OnInit } from '@angular/core';
import { TesteService } from '../teste.service';

@Component({
  selector: 'app-delete-page',
  templateUrl: './delete-page.component.html',
  styleUrls: ['./delete-page.component.scss']
})
export class DeletePageComponent implements OnInit {
  //public id: number;
  constructor(private delet: TesteService) { }

  ngOnInit(): void {
  }

  deleteTeste(id:number) {
    console.log(id)
    this.delet.deleteTeste(id).subscribe();
  }

}
